
##
## $ make && sudo make install -> install to /usr/bin/blab, and samples to /usr/share/blab
##

DESTDIR?=
PREFIX?=/usr
CFLAGS?=-Wall -O3
OFLAGS?=-O1
INSTALL?=install
OWLURL=https://haltp.org/files/ol-0.1.23.c.gz
OL?=bin/ol

bin/blab: blab.c
	mkdir -p bin
	$(CC) $(CFLAGS) -o bin/blab blab.c

everything: bin/blab seal-of-quality doc/blab.1.gz

blab.c: blab.scm lib/bisect/core.scm 
	echo "(define-library (settings) (import (owl base)) (export default-library-path) (begin (define default-library-path \"$(DESTDIR)$(PREFIX)/share/blab\")))" > settings.scm
	make check-owl
	$(OL) $(OFLAGS) -o blab.c blab.scm

install: bin/blab doc/blab.1.gz
	$(INSTALL) -d -m 755 $(DESTDIR)$(PREFIX)/bin
	$(INSTALL) -d -m 755 $(DESTDIR)$(PREFIX)/share/blab
	$(INSTALL) -d -m 755 $(DESTDIR)$(PREFIX)/share/man/man1
	$(INSTALL) -m 755 bin/blab $(DESTDIR)$(PREFIX)/bin
	$(INSTALL) -m 644 lib-blab/*.blab $(DESTDIR)$(PREFIX)/share/blab
	$(INSTALL) -m 644 doc/blab.1.gz $(DESTDIR)$(PREFIX)/share/man/man1

seal-of-quality: bin/blab
	cd tests && ./run.sh ../bin/blab
	touch seal-of-quality

doc/blab.1.gz: doc/blab.1
	cat doc/blab.1 | gzip -9 > doc/blab.1.gz

# run tests against bin/blab
test: seal-of-quality

# run tests against a bytecode image (to avoid the long C-compile part)
testi: blab.fasl
	cd tests && ./run.sh owl-vm ../blab.fasl

clean:
	-rm blab.c bin/* seal-of-quality doc/blab.1.gz settings.scm blab.fasl

mrproper: clean
	-rm -rf tmp bin

uninstall:
	rm $(DESTDIR)$(PREFIX)/bin/blab
	rm -rf $(DESTDIR)$(PREFIX)/share/blab
	rm $(DESTDIR)$(PREFIX)/share/man/man1/blab.1.gz

check-owl: 
	$(OL) --version || make get-owl

get-owl:
	mkdir -p tmp bin
	test -f tmp/ol.c || curl $(OWLURL) | gzip -d > tmp/ol.c
	cc -O2 -o $(OL) tmp/ol.c
      
.PHONY: install clean mrproper test everything uninstall testi get-owl check-owl
